$(document).ready(function() {    
    // tabs
    $('ul.js-tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.js-tabs li').removeClass('current');
		$('.js-tab-content').removeClass('current');

		$(this).addClass('current');
		$('#'+tab_id).addClass('current');
    })
    // modal open
	$('[data-popup-open]').on('click', function(e) {
		var targeted_popup_class = jQuery(this).attr('data-popup-open');
		$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
		e.preventDefault();
	});
	// modal close
	$('[data-popup-close]').on('click', function(e) {
		var targeted_popup_class = jQuery(this).attr('data-popup-close');
		$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
		e.preventDefault();
    });
    // validate form signin
    $('#signIn').click(function() {
        var errors = 0;
        $('#form-signin input').map(function(){
             if( !$(this).val() ) {
                  $(this).parents('.js-form-item').addClass('warning');
                  errors++;
            } else if ($(this).val()) {
                  $(this).parents('.js-form-item').removeClass('warning');
            }   
        });
        if(errors > 0){
            // $('#errorwarn').text("All fields are required");
            return false;
        }
    });
    // render data event 
    const renderNewEvent = ({ name, desc }) => {
    return `<li>
                <img class="thumb" src="images/img_event.jpg" alt="img">
                <p class="info">
                    <label class="title">${name}</label>
                    <span>${desc}</span>
                </p>
            </li>`;
    };
    const render = (data, selector, func) => {
    $.get(data).then(response => {
        response.forEach((ele, index) => {
        $(selector).append(func(ele, index));
        });
    });
    };
    render('data/data_events.json', '#listEvent', renderNewEvent);
});
    